#!/bin/bash

cont=0

echo BUSCANDO MÁQUINAS NA REDE...

#REALIZA UM SCAN NA REDE EM BUSCA DE TODOS OS HOSTS ONLINE
#AUMENTAR PARA 254
for (( i=170 ; i<=180 ; i++ ));
do
	#REALIZA UM PING DEZ VEZES EM CADA HOST
	for (( j=0 ; j<10 ; j++ ));
	do
		ping -c 1 -W 0.05 192.168.100.$i &>/dev/null

	done
		a=$?
		#ANALIZA SE O PING FOI BEM SUCEDIDO E CASO POSITIVO
		#ADICIONA ELE A LISTA DE ENDEREÇOS IP
		if [[ "$a" !=  "1" ]];
		then
			ipadd[$cont]="192.168.100.$i"
			cont=$(( $cont + 1 ))
			echo "$cont host(s) disponível(is)" 
		fi	
done

#CRIA A LISTA DE COMANDOS QUE SERÃO PASSADOS BASEADO NOS ENDEREÇOS IP
#OBTIDOS NA LISTA ANTERIOR
disp=""

for ((i=0; i != $cont; i++));
do
	disp+="--field='${ipadd[$i]}:fbtn' 'echo ${ipadd[$i]}' "
done

#ARMAZENA O COMANDO YAD EM UMA VARIÁVEL
main="yad --title='CONEXOES' --quoted-output --form --columns=1 --geometry=640x320 --scroll --text='Conexoes Disponiveis:' $disp --button=Cancelar:0 --button=Conectar:255"

#UTILIZA O COMANDO eval PARA RODAR CORRETAMENTE O YAD E ARMAZENA SUA SAIDA
#EM UMA VARIÁVEL: main_out
main_out=$(eval $main)

#ARMAZENA A OPCAO ESCOLHIDA PELO USUÁRIO
choice=$?

if [ $choice -eq 0 ];
then
	echo OPERACAO CANCELADA!
	exit
else
	ip=$(echo $main_out | awk '{print $NF}')
fi

ping -c 4 $ip
